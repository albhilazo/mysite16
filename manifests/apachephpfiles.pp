class mysite16::apachephpfiles
{
  # PHP
  include ::yum::repo::remi
  package { 'libzip-last':
    require => Yumrepo['remi']
  }

  class{ '::yum::repo::remi_php56':
    require => Package['libzip-last']
  }

  class { 'php':
    version => 'latest',
    require => Yumrepo['remi-php56'],
  }

  php::module { [ 'devel', 'pear', 'xml', 'mbstring', 'pecl-memcache', 'soap' ]: }




  # Apache
  class{ 'apache': }

  apache::vhost { 'myMpwar.prod':
    port    => '80',
    docroot => '/var/www/myproject',
  }

  apache::vhost { 'myMpwar.dev':
    port    => '80',
    docroot => '/var/www/myproject',
  }




  include mysite16::createfiles
}