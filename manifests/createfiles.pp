class mysite16::createfiles
{
  file { '/var/www/myproject/index.php':
    ensure  => 'present',
    content => "Hello World. Sistema operativo ${operatingsystem} ${operatingsystemrelease}",
    mode    => '0644',
  }

  file { '/var/www/myproject/info.php':
    ensure  => 'present',
    content => "<?php phpinfo();",
    mode    => '0644',
  }
}