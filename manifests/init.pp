class mysite16
{
  
  # Miscellaneous packages.
  $misc_packages = [
    'sendmail','vim-enhanced','telnet','zip','unzip','screen',
    'libssh2','libssh2-devel','gcc','gcc-c++','autoconf','automake','postgresql-libs'
  ]

  package { $misc_packages: ensure => latest }




  include mysite16::apachephpfiles




  include mysite16::mysql




  include mysite16::mongodb




  # Ensure Time Zone and Region.
  class { 'timezone':
    timezone => 'Europe/Madrid',
  }

  #NTP
  class { '::ntp':
    server => [ '1.es.pool.ntp.org', '2.europe.pool.ntp.org', '3.europe.pool.ntp.org' ],
  }

  


  # Ip Tables.
  if $operatingsystemrelease == '7.0.1406'
  {
    # firewalld - Centos 7
    firewalld_rich_rule { 'Accept HTTP':
      ensure  => present,
      zone    => 'public',
      service => 'http',
      action  => 'accept',
    }
  }
  else
  {
    package { 'iptables':
      ensure => present,
      before => File['/etc/sysconfig/iptables'],
    }
    file { '/etc/sysconfig/iptables':
      ensure  => file,
      owner   => "root",
      group   => "root",
      mode    => 600,
      replace => true,
      source  => "puppet:///modules/mysite16/iptables.txt",
    }
    service { 'iptables':
      ensure     => running,
      enable     => true,
      subscribe  => File['/etc/sysconfig/iptables'],
    }
  }
}