class mysite16::mysql
{
  # MYSQL
  class { '::mysql::server':
    root_password    => 'vagrantpass',
  }

  mysql::db { 'mympwar':
    user     => 'mpwardb',
    password => 'mpwardb',
  }

  mysql::db { 'mpwar_test':
    user     => 'mpwardb',
    password => 'mpwardb',
  }
}